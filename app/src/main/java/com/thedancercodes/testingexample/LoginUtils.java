package com.thedancercodes.testingexample;

/**
 * Created by TheDancerCodes on 28/03/2018.
 */

/**
 * This class provides some simple validation.
 */

public class LoginUtils {

    /**
     *
     * This method checks if the provided string represents a valid email address and returns true
     * if it is.
     *
     * @param email = email
     * @return - hasAtSign
     */
    public boolean isValidEmailAddress(String email) {

        // Check whether the email has an @ sign
        boolean hasAtSign = email.indexOf("@") > -1;

        return hasAtSign;

    }

    // Check the length local part of the email address. Everything before the @ sign

    /**
     *This method returns the length of the local part of an email address,
     * which is the part that comes before the "@" in the address.
     *
     * @param email = email
     * @return - the length of the local part of the email address
     */
    public int getLocalPartLength(String email) {

        int start = email.indexOf("@");
        String localPart = email.substring(0, start);
        return localPart.length();
    }

}
