package com.thedancercodes.testingexample;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * To work on unit tests, switch the Test Artifact in the Build Variants.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class LoginUtilsTest {

    private LoginUtils utils;

    @Before
    public void setUp() {
        utils = new LoginUtils();
    }

    @Test
    public void aValidEmailAddressPasses() throws Exception {

        assertTrue(utils.isValidEmailAddress("taracha@gmail.com"));
    }

    @Test
    public void anInvalidEmailAddressFails() throws Exception {

        assertFalse(utils.isValidEmailAddress("invalid"));
        // assertTrue(!utils.isValidEmailAddress("invalid"));
    }

    @Test
    public void localPartLengthForValidEmailAddress() throws Exception {

        assertEquals(1, utils.getLocalPartLength("a@b.com"));
    }
}